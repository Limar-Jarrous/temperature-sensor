#include <16F877A.h>
#device adc=10
#use delay(clock=4M)

int i_code,j_code;
int m,n;

int code(int x)
{
	if (x==0) return(0x3F);
	if (x==1) return(0x06);
	if (x==2) return(0x5B);
	if (x==3) return(0x4F);
	if (x==4) return(0x66);
	if (x==5) return(0x6D);
	if (x==6) return(0x7D);
	if (x==7) return(0x07);
	if (x==8) return(0x7F);
	if (x==9) return(0x6F);
}


void warning_2(int16 value){
    int x;

    m=value%10;
    value/=10;
    n=value%10;

    j_code=code(n);
    i_code=code(m);


    for (x=0;x<=70;x++){
		output_high(pin_b1);output_low(pin_b2);output_c(i_code);
		delay_us(500);
		output_high(pin_b2);output_low(pin_b1);output_c(j_code);
		delay_ms(2);
    }

    output_high(pin_d1); // led on
}

void warning_1(int16 value){
    int x;

    m=value%10;
    value/=10;
    n=value%10;

    j_code=code(n);
    i_code=code(m);


    for (x=0;x<=200;x++){
		output_high(pin_b1);output_low(pin_b2);output_c(i_code);
		delay_us(500);
		output_high(pin_b2);output_low(pin_b1);output_c(j_code);
		delay_ms(1);
    }

    output_high(pin_d1); // led on
    delay_ms(40);
    output_low(pin_d1); //  led off
}

void main() {
	int16 value;
	int limit2=40;
	int limit1=20;

	delay_ms(50);


	setup_adc_ports(RA0_ANALOG);
	setup_adc(ADC_CLOCK_DIV_8);

	while(1){
		value = read_adc()/2.04;
		if( value >= limit2 )     { warning_2(value);  }
		else if( value >= limit1 ){ warning_1(value);  }
		else {
			output_low(pin_d1);//  led off
		}
		delay_ms(1);
	}
}
