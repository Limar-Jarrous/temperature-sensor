#include <16F877A.h>
#device adc=10
#use delay(clock=4M)

char string[16];

Char text[2];

void pulse(){
output_high(PIN_E0);
delay_us(100);
output_low(PIN_E0);}

void lcd_clear(){
   output_low(PIN_E1);
   OUTPUT_C(0X01);PULSE();
   delay_ms(10);
}

void lcd_initial(){
   output_low(PIN_E1);
   OUTPUT_C(0X38);PULSE();
   OUTPUT_C(0X0c);PULSE();
   OUTPUT_C(0X06);PULSE();
   lcd_clear();
}

void lcd_write(int x){
   int i;
   output_low(PIN_E1);
   //initialize the screen pointer at the first row and the first column
   if (x==1){OUTPUT_C(0X80);PULSE();}
   //initialize the screen pointer at the second row and the first column
   if (x==2){OUTPUT_C(0XC0);PULSE();}
   output_high(PIN_E1);
   for (i=0 ; i<=15; i++){
   output_C(string[i]); PULSE();}
}

void warning_2(int16 value){
   //lcd_clear();
   strcpy(string,"  It's Too Hot  ");
   lcd_write(1);
   strcpy(string,"                ");
   sprintf(text,"%lu",value);
   string[6]=text[0]; string[7]=text[1];
   lcd_write(2);

   output_high(pin_d1); // led on

}

void warning_1(int16 value){
  // lcd_clear();
   strcpy(string,"TEMPERATURE IS ");
   lcd_write(1);
   strcpy(string,"                ");
   sprintf(text,"%lu",value);
   string[6]=text[0]; string[7]=text[1];
   lcd_write(2);

   output_high(pin_d1); // led on
   delay_ms(500);
   output_low(pin_d1); //  led off
}

void main() {
int16 value;
int limit2=40;
int limit1=20;

delay_ms(50);
lcd_initial();

setup_adc_ports(RA0_ANALOG);
setup_adc(ADC_CLOCK_DIV_8);

while(1){
   value = read_adc()/2.04;
   if( value >= limit2 )     { warning_2(value);  }
   else if( value >= limit1 ){ warning_1(value);  }
   else {
      strcpy(string,"TEMPERATURE IS ");
      lcd_write(1);
      strcpy(string,"               ");
      sprintf(text,"%lu",value);
      string[6]=text[0]; string[7]=text[1];
      lcd_write(2);

      output_low(pin_d1);//  led off
   }
   delay_ms(300);
}
}
