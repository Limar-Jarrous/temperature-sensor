# Temperature-Sensor

Project: Temperature sensor

Material: Measurements and Measuring Devices


## Used equipments:

• LCD screen (2x16)

• Microcontroller (PIC 16F877A)

• Crystal (4MHz)

• Temperature Sensor (LM35)

• LED

• (10KΩ) and (220Ω) resistances

• Variable resistance

• Continuous feeding source (5V)

## Working principle:

The sensor receives the ambient temperature after it is connected to a feed source and outputs the temperature in the form of a similar signal and is sent to the microcontroller to convert it to a digital signal. The temperature value is then printed on the LCD screen.
If the temperature is between [20-40]°C the LED turns on and off.
If the temperature is equal or more than 40°C, the LED remains on.
Screen illumination is controlled by a variable resistance.

## The Circuit
![circuit diagram](https://user-images.githubusercontent.com/57095100/71132630-b7f73f80-2200-11ea-9d90-aaa234ea9544.jpg)
